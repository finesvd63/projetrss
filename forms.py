from flask_wtf import FlaskForm
from wtfpeewee.orm import model_form
from wtforms.fields import PasswordField, TextField
from wtforms.validators import DataRequired, Length
from wtforms.widgets import PasswordInput
from models import Users, Flux



class LoginForm(FlaskForm):
    username = TextField('Pseudo: ', validators=[DataRequired(), Length(min=3, max=20)])
    password = PasswordField('Mot de passe: ', widget=PasswordInput(hide_value=True))
    pass

class RegisterForm(FlaskForm):
    username = TextField('Pseudo: ', validators=[DataRequired(), Length(min=3, max=20)])
    password = PasswordField('Mot de passe: ', widget=PasswordInput(hide_value=True))
    pass


class AjoutFlux(FlaskForm):
    name_flux = TextField('Nom du flux: ', validators=[DataRequired()])
    address_flux = TextField('Lien du flux: ', validators=[DataRequired()])
    pass
