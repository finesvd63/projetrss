from flask import Flask, flash, redirect, render_template, request, session, abort, url_for
from flask_login import login_required, login_user, logout_user, LoginManager, current_user
from flask_mobility import Mobility
from werkzeug.security import generate_password_hash, check_password_hash
from models import Users, Flux, create_tables, drop_tables, create_tables2, drop_tables2
from forms import LoginForm, RegisterForm, AjoutFlux

import os, click, feedparser

app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "do_admin_login"

@login_manager.user_loader
def load_user(user_id):
    return Users.get(user_id)

@app.route('/')
def home():
    list_flux_data = feedparser.parse('https://www.lemonde.fr/rss/une.xml')
    return render_template('index.html', list_flux = list_flux_data)


@app.route('/login', methods=['GET', 'POST'])
def do_admin_login():
    form = LoginForm()
    if form.validate_on_submit():
        user = Users.select().where(Users.username == form.username.data).first()
        if (user != None):
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('home'))
            else:
                print("Le mode de passe ou le pseudo est incorrect")
        else:
                print("Le mode de passe ou le pseudo est incorrect")
    return render_template('login.html', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    user = Users()
    form = RegisterForm()
    user = Users.select().where(Users.username == form.username.data).first()
    if (user == None):
        if form.validate_on_submit():
            user.username = form.username.data
            user.password = generate_password_hash(form.password.data)
            user.save()
            flash('Utilisateur créé !')
            return redirect(url_for('do_admin_login'))
    return render_template('register.html', form=form)


@app.route("/logout")
@login_required
def logout():
    #session['logged_in'] = False
    logout_user()
    return home()



@app.route("/account", methods=['GET', 'POST'])
@login_required
def flux():
    form = AjoutFlux()
    if form.validate_on_submit():
        name_flux = form.name_flux.data
        address_flux = form.address_flux.data
        print(name_flux)
        print(address_flux)
        if(Flux.select().where((Flux.flux == address_flux) & Flux.id_users == current_user.id).first() == None):
            newflux = Flux()
            newflux.name = name_flux
            newflux.id_users = current_user.id
            newflux.flux = address_flux
            newflux.save()
            return flux()
    fluxUser = Flux.select().where(Flux.id_users_id == current_user.id)
    return render_template('account.html', form=form, fluxUser=fluxUser)


@app.route("/myflux", methods=['GET', 'POST'])
@login_required
def visionnerMonFlux():
    idFluxUser = request.args.get('id_flux')
    fluxUser = Flux.select().where(Flux.id == idFluxUser).first()
    list_flux_data = feedparser.parse(fluxUser.flux)
    return render_template('myflux.html', nameFlux = fluxUser.name, list_flux = list_flux_data)


@app.route("/deleteflux", methods=['GET', 'POST'])
@login_required
def deleteflux():
    idFluxUser = request.args.get('id_flux')
    requete = Flux.delete().where((Flux.id_users == current_user.id) & (Flux.id == idFluxUser))
    requete.execute()
    return flux()


##############################################################
# DATABASE
##############################################################

@app.cli.command()
def initdbUser():
    """Create databaseUser"""
    create_tablesUser()

@app.cli.command()
def initdbFlux():
    """Create databaseFlux"""
    create_tablesFlux()

@app.cli.command()
def dropdbUser():
    """Drop database tables User"""
    drop_tablesUser()

@app.cli.command()
def dropdbFlux():
    """Drop database tables Flux"""
    drop_tablesFlux()
